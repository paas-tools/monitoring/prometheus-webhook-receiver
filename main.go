package main

import (
	"encoding/json"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"
	"os"

	batchv1 "k8s.io/api/batch/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"github.com/ghodss/yaml"
)

type (

	// Timestamp is a helper for (un)marhalling time
	Timestamp time.Time

	// HookMessage is the message we receive from Alertmanager
	HookMessage struct {
		Version           string            `json:"version"`
		GroupKey          string            `json:"groupKey"`
		Status            string            `json:"status"`
		Receiver          string            `json:"receiver"`
		GroupLabels       map[string]string `json:"groupLabels"`
		CommonLabels      map[string]string `json:"commonLabels"`
		CommonAnnotations map[string]string `json:"commonAnnotations"`
		ExternalURL       string            `json:"externalURL"`
		Alerts            []Alert           `json:"alerts"`
	}

	Alert struct {
		Labels      map[string]string `json:"labels"`
		Annotations map[string]string `json:"annotations"`
		StartsAt    string            `json:"startsAt,omitempty"`
		EndsAt      string            `json:"EndsAt,omitempty"`
	}

	cliSet struct {
		clientset kubernetes.Clientset
		job_destination_namespace string
		configmap_namespace string
		responses_configmap string
	}
)

func main() {
	log.Printf("Starting webhook receiver")

	// Extract the current namespace from the mounted secrets
	default_k8s_namespace_location := "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
	if _, err := os.Stat(default_k8s_namespace_location); os.IsNotExist(err) {
		log.Printf("Current kubernetes namespace could not be found")
		panic(err.Error())
	}

	namespace_dat, err := ioutil.ReadFile(default_k8s_namespace_location)
	current_namespace := string(namespace_dat)

	// Overwrite the job namespace destination, if ommited, current namespace will be used
	configmap_namespace := flag.String("configmap_namespace", "", "Openshift namespace where jobs are defined")
	job_destination_namespace := flag.String("job_destination_namespace", current_namespace, "Openshift namespace where jobs will be created")
	// configMap where job definitions are stored
	responses_configmap := flag.String("responses_configmap", "receiver-job-definitions", "Configmap containing response YAML job definitions")
	addr := flag.String("addr", ":9270", "address to listen for webhook")
	flag.Parse()

	if *configmap_namespace == "" {
		// We did not receive any configMap namespace parameter
		log.Printf("configmap_namespace is required")
		os.Exit(1)
	}

	// Creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	c := &cliSet{
		clientset: *clientset,
		job_destination_namespace: *job_destination_namespace,
		responses_configmap: *responses_configmap,
		configmap_namespace: *configmap_namespace,
	}

	http.HandleFunc("/healthz", healthzHandler)
	http.HandleFunc("/alerts", c.alertsHandler)
	log.Fatal(http.ListenAndServe(*addr, nil))
}

func healthzHandler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Webhook receiver is running\n")
}

func (c *cliSet) alertsHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		c.postHandler(w, r)
	case http.MethodGet:
		c.getHandler(w, r)
	default:
		http.Error(w, "unsupported HTTP method: "+string(r.Method), 400)
	}
}

func (c *cliSet) postHandler(w http.ResponseWriter, r *http.Request) {

	dec := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var m HookMessage
	if err := dec.Decode(&m); err != nil {
		log.Printf("error decoding message: %v", err)
		http.Error(w, "invalid request body", 400)
		return
	}

	status := m.Status
	alarm_name := m.CommonLabels["alertname"]

	log.Printf("Alarm received: " + alarm_name + "[" + status + "]")

	for k, v := range m.CommonLabels {
		log.Printf("Label key[%s] value[%s]\n", k, v)
	}
	for k, v := range m.CommonAnnotations {
		log.Printf("Annotation key[%s] value[%s]\n", k, v)
	}

	// Extract the key to look for on the responses configMap
	firing_job_key := m.CommonAnnotations["firing_job"]
	resolved_job_key := m.CommonAnnotations["resolved_job"]

	//Decide wheter it is a firing alarm or a resolved one, depending on the used annotation
    if len(firing_job_key) > 0 && status == "firing" {
		c.createResponseJob(firing_job_key, w)
	} else if (len(resolved_job_key) > 0 && status == "resolved") {
		c.createResponseJob(resolved_job_key, w)
	} else {
		log.Printf("Received alarm without correct response configuration, ommiting reponses.")
		return
	}
}

func (c *cliSet) getHandler(w http.ResponseWriter, r *http.Request) {
	//Alertmanager expects an 200 OK response, otherwise send_resolved will never work
	enc := json.NewEncoder(w)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if err := enc.Encode("OK"); err != nil {
		log.Printf("error encoding messages: %v", err)
	}
}

func (c *cliSet) createResponseJob(job_key string, w http.ResponseWriter) {

    configMap, err := c.clientset.CoreV1().ConfigMaps(c.configmap_namespace).Get(c.responses_configmap, metav1.GetOptions{})
	if err != nil {
		log.Printf("error while retrieving the configMap: " + c.responses_configmap, err)
		http.Error(w, "Webhook error retrieving configMap with job definitions", 500)
		return
	}

	job_definition := configMap.Data[job_key]
	yaml_job_definition := []byte(job_definition)

	// yaml_job_definition contains a []byte of the yaml job spec
	// convert the yaml to json so it works with Unmarshal
	jsonBytes, err := yaml.YAMLToJSON(yaml_job_definition)
	if err != nil {
		log.Printf("error while converting YAML job definition to JSON: %v", err)
		http.Error(w, "Webhook error creating a job", 500)
		return
	}

	obj := &batchv1.Job{}
	err = json.Unmarshal(jsonBytes, obj)

	if err != nil {
		log.Printf("Error while using unmarshal on received job: %v", err)
		http.Error(w, "Webhook error creating a job", 500)
		return
	}

	// Job client for creating the job according to the job definitions extracted from the responses configMap
	jobsClient := c.clientset.BatchV1().Jobs(c.job_destination_namespace)

	// Create job
	log.Printf("Creating job... ")
	result, err := jobsClient.Create(obj)
	if err != nil {
		log.Printf("error creating job: %v", err)
		http.Error(w, "Webhook error creating a job", 500)
		return
	}
	log.Printf("Created job -->\n %q.", result)
}