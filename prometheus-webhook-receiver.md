# Prometheus-webhook-receiver

`prometheus-webhook-receiver` is a simple webhook receiver for the [Prometheus Alertmanager](https://prometheus.io/docs/alerting/alertmanager/). It expect to receive
alerter messages in POST bodies to `/alerts` in JSON in the format described in
the [receiver webhook docs](https://prometheus.io/docs/alerting/configuration/#webhook-receiver-<webhook_config>)

Alarm responses must be stored on a configMap (by default named `receiver-job-definitions`). Each entry stores a job response, identified with a unique key.

In order to respond to an specific alarm, it must have one or both of the following annotations:

* `firing_job`: This annotation will be used as the key on the responses configMap that corresponds to the YAML job definition to run when the alarm status is `firing`.
* `resolved_job`: This annotation will be used as the key on the responses configMap that corresponds to the YAML job definition to run when the alarm status is `resolved`.

## Resources

The webhook receiver requires to run as a deployment using a service account with permissions to list and create jobs on the destination namespace, as well as reading configMaps.

## Usage

`prometheus-webhook-receiver` creates jobs on the destination namespace as response to Prometheus alerts.

```bash
$ ./main
Usage of prometheus-webhook-receiver
  -addr string
    	address to listen for webhook (default ":9270")
  -job_destination_namespace string
    	Openshift namespace where jobs will be created (default "CURRENT_NAMESPACE")
  -responses_configmap string
      Configmap containing response YAML job definitions (default "receiver-job-definitions")
exit status 2
```

It expects alerts to be POSTed in JSON to `/alerts`.

## Helm Deployment

**NB: This Helm chart should be deployed as a subchart as part of a higher-level application deployment.**

This chart is already part of [oo-gitlab-cern](https://gitlab.cern.ch/vcs/oo-gitlab-cern) Helm deployment, using this project as a submodule.
